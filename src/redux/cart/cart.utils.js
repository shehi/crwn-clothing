export const addItemToCart = (cartItems, cartItemToAdd)=>{
    const exists = cartItems.find(x=>x.id ===cartItemToAdd.id)
    if(exists){
        return cartItems.map(itm=> itm.id === cartItemToAdd.id?({...itm, qty : itm.qty + 1}):itm
        )
    }
    return [...cartItems,{...cartItemToAdd,qty:1}]
}


export const removeItemFromCart = (cartItems, cartItemToRemove)=>{
    const exists = cartItems.find(x=>x.id ===cartItemToRemove.id)
    if(exists.qty === 1){
        return  cartItems.filter(x=>x.id !==cartItemToRemove.id ) 
    }
    return cartItems.map(itm=> itm.id === cartItemToRemove.id?({...itm, qty : itm.qty -1 }):itm
    )
}