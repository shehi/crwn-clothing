import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyBskOZpDyF6njlBr-80FQrImJUyFv69r_4",
  authDomain: "crown-db-c1cc7.firebaseapp.com",
  projectId: "crown-db-c1cc7",
  storageBucket: "crown-db-c1cc7.appspot.com",
  messagingSenderId: "329691376538",
  appId: "1:329691376538:web:cedd6c2ae42a6f9f226823",
  measurementId: "G-6ZRKGYE9PR"
};
 

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};


export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;