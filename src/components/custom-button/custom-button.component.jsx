import React from 'react';

import './custom-buttom.styles.scss';

const CustomButton = ({ children,isGoogle,inverted, ...otherProps }) => (
  <button className={`custom-button ${isGoogle?'google-sign-in':''} ${inverted?'inverted':''}`} {...otherProps}>
    {children}
  </button>
);

export default CustomButton;

