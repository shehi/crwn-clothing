import React from 'react';
import FormInput from '../form-input/form-input.component';
import CustomButton from '../custom-button/custom-button.component';
import './sign-up.styles.scss';

import {auth, createUserProfileDocument} from '../../firebase/firebase.utils'


class SignUp extends React.Component {
  constructor() {
    super();

    this.state = {
      displayName: '',
      email: '',
      password: '',
      confirmPassword: '',
      success: null
    };
  }

  handleSubmit = async event => {
    event.preventDefault();

    const { displayName, email, password, confirmPassword } = this.state;

    if (password !== confirmPassword) {
      alert("passwords don't match");
      return;
    }

    try {
      const { user } = await auth.createUserWithEmailAndPassword(
        email,
        password
      );

      await createUserProfileDocument(user, { displayName });

      this.setState({
        displayName: '',
        email: '',
        password: '',
        confirmPassword: ''
      });
      this.setState({success:true})
    } catch (error) {
      this.setState({success:false})
      console.error(error);
    }
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };


  render() {
    let message = ''
    const {success} = this.state
    if( success !== null){
      message = success? 'User registered successfully' : 'Error registering user'
    }
  
    return (
      <div className='sign-up'>
        <h2>I do not have an account</h2>
        <span>Sign up with your email and password</span>

        <form onSubmit={this.handleSubmit}>
        <FormInput
            name='displayName'
            type='text'
            handleChange={this.handleChange}
            value={this.state.displayName}
            label='display name'
            required
          />
          
            <FormInput
            name='email'
            type='email'
            handleChange={this.handleChange}
            value={this.state.email}
            label='email'
            required
          />
          <FormInput
            name='password'
            type='password'
            value={this.state.password}
            handleChange={this.handleChange}
            label='password'
            required
          />
            <FormInput
            name='confirmPassword'
            type='password'
            value={this.state.confirmPassword}
            handleChange={this.handleChange}
            label='confirm password'
            required
          />
          {message}
          <div className='buttons'>
            <CustomButton type='submit'> Sign up </CustomButton>
            {/* <CustomButton onClick={SignInWithGoogle} isGoogle> Sign in with Google </CustomButton> */}
          </div>
        </form>
      </div>
    );
  }
}

export default SignUp;